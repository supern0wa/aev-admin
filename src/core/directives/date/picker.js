import rome from 'rome';

export default {
	bind (el, binding, vnode) {
		el.addEventListener('keydown', e => e.preventDefault);
	},

	inserted (el, binding, vnode) {
		el.picker = rome(el, {
			inputFormat: binding.value ? binding.value : 'YYYY-MM-DD',
			time: false
		});

		el.picker.on('data', () => el.dispatchEvent(new Event('input')));
		el.picker.on('show', () => {
			let next = el.picker.container.querySelector('.rd-next'),
				prev = el.picker.container.querySelector('.rd-back');

			next.classList.add('md-button', 'md-icon-button');
			next.innerHTML = '<i class="md-icon material-icons">arrow_forward</i>';

			prev.classList.add('md-button', 'md-icon-button');
			prev.innerHTML = '<i class="md-icon material-icons">arrow_back</i>';
		});
	},

	update (el) {
		el.picker.setValue(el.value);
	},

	unbind (el, binding, vnode) {
		el.removeEventListener('keydown', e => e.preventDefault);
		el.picker.destroy();
	}
}
