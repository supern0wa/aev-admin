import InstitutionForm from './InstitutionForm';

export default [
	{
		path: '/institutions/edit/:id',
		component: InstitutionForm,
		meta: { requiresAuth: true }
	}
]
