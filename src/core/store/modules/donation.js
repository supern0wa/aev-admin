import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getDonations (context, payload) {
			return Vue.http.get(process.env.API_URL + '/donations',
				{ headers: getHeaders() }
			);
		},
		getDonation (context, payload) {
			return Vue.http.get(process.env.API_URL + '/donations/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextDonation(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
