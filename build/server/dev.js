var path = require('path'),
	app = require('../../server'),
	webpack = require('webpack'),
	webpackConfig = require('../webpack.dev'),
	devMiddleware = require('webpack-dev-middleware'),
	hotMiddleware = require('webpack-hot-middleware'),
	history = require('connect-history-api-fallback');

const compiler = webpack(webpackConfig),
	devMiddlewareInstance = devMiddleware(compiler, {
		noInfo: true,
		publicPath: '/',
		index: 'index.html',
		watchOptions: {
			aggregateTimeout: 300,
			poll: 1000 
		}
	}),
	hotMiddlewareInstance = hotMiddleware(compiler, {});

app.use(history());
app.use(devMiddlewareInstance);
app.use(hotMiddlewareInstance);
