import PageList from './PageList';
import PageForm from './PageForm';

export default [
	{
		path: '/pages',
		component: PageList,
		meta: { requiresAuth: true }
	},
	{
		path: '/pages/add',
		component: PageForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/pages/edit/:id',
		component: PageForm,
		meta: { requiresAuth: true }
	}
]
