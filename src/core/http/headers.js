import { store } from '../';

export default function () {
	const access_token = localStorage.getItem('access_token');

	const headers = {
		'Accept': 'application/json',
		'Authorization': 'Bearer ' + access_token
	}

	return headers;
}
