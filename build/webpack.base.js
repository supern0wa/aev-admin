var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

const resolvePath = dir => path.resolve(__dirname, '..', dir);

module.exports = {
	entry: './src/main.js',
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {
		}
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				exclude: '/node_modules',
				options: {
					loaders: {
						sass: [
							'vue-style-loader',
							'css-loader',
							'sass-loader',
							{
								loader: 'sass-resources-loader',
								options: {
									resources: resolvePath('src/core/stylesheet/main.scss')
								}
							}
						]
					}
				}
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: '/node_modules',
				include: [
					resolvePath('src')
				]
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.json$/,
				use: ['json-loader']
			},
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			moment: 'moment'
		}),
		new webpack.EnvironmentPlugin({
			NODE_ENV: 'development',
			API_URL: null,
			AUTH_URL: null,
			BASE_PATH: '',
			STORAGE_URL: null,
			CLIENT_ID: null,
			CLIENT_SECRET: null,
			CLIENT_SCOPE: null
		}),
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'src/index.html',
			title: 'Admin',
			inject: true,
		})
	]
};
