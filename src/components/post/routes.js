import PostList from './PostList';
import PostForm from './PostForm';

export default [
	{
		path: '/posts',
		component: PostList,
		meta: { requiresAuth: true }
	},
	{
		path: '/posts/add',
		component: PostForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/posts/edit/:id',
		component: PostForm,
		meta: { requiresAuth: true }
	}
]
