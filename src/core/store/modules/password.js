import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		email(context, payload) {
			return Vue.http.post(process.env.API_URL + '/password/email',
				payload
			);
		},

		reset(context, payload) {
			return Vue.http.post(process.env.API_URL + '/password/reset',
				payload
			);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}

