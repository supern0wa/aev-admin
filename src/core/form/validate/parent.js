import Vue from 'vue';

export default {
	methods: {
		submit (callback) {
			// Subscribe to child's error-changed event
			this.$root.$once('errors-changed', success => {
				if (success)
					callback();
			})

			// Validates all child components
			this.validate();
		},
		validate () {
			this.$root.$emit('validate');
		}
	}
}
