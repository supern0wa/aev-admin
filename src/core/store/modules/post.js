import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getPosts (context, payload) {
			return Vue.http.get(process.env.API_URL + '/posts',
				{ headers: getHeaders() }
			);
		},
		addPost (context, payload) {
			return Vue.http.post(process.env.API_URL + '/posts',
				payload,
				{ headers: getHeaders() }
			)
		},
		getPost (context, payload) {
			return Vue.http.get(process.env.API_URL + '/posts/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editPost (context, payload) {
			return Vue.http.put(process.env.API_URL + '/posts/' + payload.post.id,
				payload,
				{ headers: getHeaders() }
			);
		},
		deletePost (context, payload) {
			return Vue.http.delete(process.env.API_URL + '/posts/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPost(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
