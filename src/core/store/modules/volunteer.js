import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getVolunteers (context, payload) {
			return Vue.http.get(process.env.API_URL + '/volunteers',
				{ headers: getHeaders() }
			);
		},
		searchVolunteers (context, payload) {
			return Vue.http.get(process.env.API_URL + `/volunteers/search?q=${payload.q}`,
				{ headers: getHeaders() }
			);
		},
		addVolunteer (context, payload) {
			return Vue.http.post(process.env.API_URL + '/volunteers',
				{
					person: Object.assign({}, payload.person, {
						birthday: moment(payload.person.birthday, 'DD/MM/YYYY').format('YYYY-MM-DD')
					}),
					volunteer: payload.volunteer,
					address: payload.address
				},
				{ headers: getHeaders() }
			)
		},
		getVolunteer (context, payload) {
			return Vue.http.get(process.env.API_URL + '/volunteers/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editVolunteer (context, payload) {
			return Vue.http.put(process.env.API_URL + '/volunteers/' + payload.volunteer.id,
				{
					person: Object.assign({}, payload.person, {
						birthday: moment(payload.person.birthday, 'DD/MM/YYYY').format('YYYY-MM-DD')
					}),
					volunteer: payload.volunteer,
					address: payload.address
				},
				{ headers: getHeaders() }
			);
		},
		deleteVolunteer (context, payload) {
			return Vue.http.delete(process.env.API_URL + '/volunteers/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
