var webpack = require('webpack');
var merge = require('webpack-merge');
var baseWebpackConfig = require('./webpack.base');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');

module.exports = merge(baseWebpackConfig, {
	devtool: 'cheap-module-eval-source-map',
	entry: [
		'webpack-hot-middleware/client',
		'./src/main.js'
	],
	output: {
		path: path.join(__dirname, '../public'),
		publicPath: process.env.BASE_PATH ? process.env.BASE_PATH : '/',
		filename: '[name].bundle.js'
	},
})
