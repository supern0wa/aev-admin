import Vue from 'vue';
import Admin from './Admin';
import { core, router, store } from './core';

Vue.use(core);

new Vue({
	el: '#admin',
	router,
	store,
	render: createElement => createElement(Admin)
});
