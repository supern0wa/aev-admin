import { store } from '../../core';

module.exports = router => {
	router.beforeEach((to, from, next) => {
		var access_token = localStorage.getItem('access_token'),
			expires_at = localStorage.getItem('expires_at'),
			refresh_token = localStorage.getItem('refresh_token');

		if (to.matched.some(record => record.meta.requiresAuth)) {
			// Reject if there's no token
			if (!access_token) {
				next({ path: '/auth' })
			}

			// Token expired
			if (!expires_at || new Date(parseInt(expires_at)) < new Date()) {
				// Use refresh token if available
				if (refresh_token) {
					store.dispatch('oauth/getToken', {
						grant_type: 'refresh_token',
						refresh_token: refresh_token,
						callback: () => next()
					});
				} else {
					// Reject
					next({ path: '/auth' })
				}
			} else {
				// Get userinfo if authenticated
				if (refresh_token) {
					var user = store.state.auth.user;

					// Verify userinfo
					if (!user)
						store.dispatch('auth/userInfo')
							.then(response => {
								store.commit('auth/setUser', response.body);
								next();
							});
					else
						next();

				} else {
					// Token is valid
					next();
				}
			}
		} else {
			// No authentication needed
			next();
		}
	});
}
